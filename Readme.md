So I'm going to experiment with the software for my DS's R4 card. Some things to note:

-Most files are either binaries or bmp
-Current file structure is small and unclear

Apparently it comes with 3 "Accessories"(Check disk for NDS; Image Viewer; Morning timer).

Supported file formats for Morning Multimedia player:
.mp1-3 .ogg .spc .tta .wav
One thing I noticed about the player is that there is no way to go back to the software menu without exiting to the DS Home.

What I expect to do soon:
-Customize a skin
-Organize/rename directories according to my understanding.
